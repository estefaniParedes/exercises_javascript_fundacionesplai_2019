import React from "react";

import { Table} from 'reactstrap';
import { Link } from "react-router-dom";

export default class Lista extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {// mapa que me crea las filas con los datos, una tabla


        //para crear las filas hacemos un "map" de los contactos recibidos
        //previamente los ordenamos por id...
        // si no pongo sort , lo pone al final  sort((a,b) => a.id-b.id): ordena la lista de contactos a traves de su id
        //sort((a,b) => a.id-b.id): ordenar un objeto-> hay que pasarle una function a.id.b.id para que sepa como tiene que ordenar
        let filas = this.props.asdf.sort((a,b) => a.id-b.id).map(contacto => {
            return (
                <tr key={contacto.id}>
                    <td>{contacto.id}</td>
                    <td>{contacto.nombre}</td>
                    <td>{contacto.email}</td>
                    <td><Link to={"/modifica/" + contacto.id}>Editar</Link></td>
                    <td><Link to={"/elimina/" + contacto.id}>Eliminar</Link></td> 
                </tr>
            );
        })


        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        );
    }


}

//<td><Link to={"/elimina/" + contacto.id}>Eliminar</Link></td> //contacto cada uno de los elementos de la lista//