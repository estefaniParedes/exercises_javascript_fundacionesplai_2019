
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

// clase idéntica a NuevoContacto
// excepto que recibimos los datos de un contacto existente
// y la lista completa de contactos
class ModificaContacto extends Component {
    constructor(props) {
        super(props);

        //recibimos id a modificar, los que llegan de /:idcontacto; para props que nos llegan desede la url match.params
        let id = this.props.match.params.idContacto * 1;
        //obtenemos el contacto a modificar
        let contactoModificar = this.props.contactos.filter(el => el.id===id)[0];
        //establecemos state con los datos del contacto a modificar
        //incluido id
        this.state = {
            nombre: contactoModificar.nombre, // epiezo con el nombre y el email y me guaro el id
            email: contactoModificar.email,
            id: contactoModificar.id,
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    cambioInput(event) { // detecta que se ha cambiado 
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    
    // el método submit envia también el id para que guardaContacto sepa qué contacto modificar
    submit(e) {
        this.props.guardaContacto({
            nombre: this.state.nombre,
            email: this.state.email,
            id: this.state.id // aqui se cual sera
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/lista' />
        }

        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nombreInput">Nombre</Label>
                            <Input type="text" 
                                name="nombre" 
                                id="nombreInput"
                                value={this.state.nombre}  // mostrara el valor que hemos modificado 
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>                        
                    </Col>
                </Row>
            </Form>

        );
    }
}



// <Button type="botton" >¬5para que no se dispare,


export default ModificaContacto;
