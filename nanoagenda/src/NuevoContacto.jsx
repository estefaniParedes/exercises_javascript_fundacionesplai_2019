
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

// recibe una enlace a la funtion
class NuevoContacto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '', // inicializar   con cambioInput se queda sincornizado con los valoresd el input
            email: '',
            volver: false // 
        };

        this.cambioInput = this.cambioInput.bind(this); // responsable que el valor nombre del state correspondan , value vincula varibale name, onchange: metodo ejectar cuando se cambie ese valor 
        this.submit = this.submit.bind(this);
    }

    //gestión genérica de cambio en campo input
    // onchange recibe un event 
    cambioInput(event) {// evento: se ha modificado un campo input
        const target = event.target; // miro que evento es  obtien el name y el value ; apunta al objeto que ha generdao el inpupt
        const value = target.type === 'checkbox' ? target.checked : target.value;//checkbox tipo de input 
        const name = target.name; // me dara email, o nombre 

        this.setState({ //  caundo ek nombre de la propiedadad es un varibales poner entre [], name se sustituira por nombre o value segun lo que se haya modificado
            [name]: value  // name sera email o nombre, por eso pongo[]: para que me modifique email o nombre
        });
    }

    //método activado al enviar el form (submit)
    //<Form onSubmit={this.submit}>// onSubmit evento predefinido // cuadno te envien ejecta esta funtion
    submit(e) { // guarda los datos de contacto, al pulsar el botton envio los datos a guaardaContacto  
        this.props.guardaContacto({
            nombre: this.state.nombre,
            email: this.state.email,
            id: 0  // no se cual sera , cuendo tepaso un  0 significa que es nuevo, me he puesto de acuerdo function contact
        });
        e.preventDefault(); // vinculado a e, abortar-detener el envio para evitar que el navegador por su cuenta envien el formulario, como estamos dentro de la funtion submit tomammos el control nosotros
       // poner que siempre que formulamos en javascript,e
        this.setState({ volver: true });//  volver: false estaba // 
    }

    render() {
        //si se activa volver redirigimos a lista
        if (this.state.volver === true) {
            return <Redirect to='/lista' /> // te es true te vas a la lista , Redirect: es un link a /lista 
        }
// onSubmit={this.submit} vinculamos al submit// onSubmit evento predefinido // cuadno te envien ejecta esta funtion
        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nombreInput">Nombre</Label>
                            <Input type="text" 
                                name="nombre" 
                                id="nombreInput"
                                value={this.state.nombre} //me refleje el valor del state  onChange={this.cambioInput} /> // metodo que se dispara cuando modifico el valor de un campo input
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} /> 
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col> 
                        <Button color="primary">Guardar</Button> 
                    </Col>
                </Row>
            </Form>

        );
    }
}






export default NuevoContacto;
// si no decis lo contrario, por defacto envia-> type="button"
//<Button color="primary">Guardar</Button> 