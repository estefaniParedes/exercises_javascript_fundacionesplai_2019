import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col ,Button} from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Inicio from './Inicio';
import Lista from './Lista';
import NuevoContacto from './NuevoContacto';
import ModificaContacto from './ModificaContacto';
import EliminaContacto from './EliminaContacto';
import P404 from './P404';// codigo de pagina no encontrada , 200 pagina encontrada

// importamos css   // si ponemos app o index lo tenenmos en toda la aplicacion 
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css'; // s pueden cargar en app

// importamos modelo
import Contacto from './Contacto';

// clase App        modifica<-app->nuevo
export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      contactos: [], //inicilaizar
      ultimoContacto: 0 // pq estamos empezando con la lista vacia
    }
//LOS DATOS ya los tenemos en a inicliazacion, en la cupula, aso los puedo llamar 
    this.guardaContacto = this.guardaContacto.bind(this);
    this.eliminaContacto = this.eliminaContacto.bind(this);
    this.cargaContactos = this.cargaContactos.bind(this);
    this.saveData = this.saveData.bind(this);
    this.loadData = this.loadData.bind(this);
    this.cargaContactos();
  }
  // guardado de datos
saveData(){
  var jsonData = JSON.stringify(this.state);
  localStorage.setItem("nanoagenda_data", jsonData);
}
         
//carga de datos
loadData(){
  var text = localStorage.getItem("nanoagenda_data");
  if (text){
      var obj = JSON.parse(text);
      this.setState(obj);
  }
}


  cargaContactos(){// crear un array con 3 contactos
    const contactosInicio = [
      new Contacto(1, "indiana", "indiana@jones.com"), // creo un contacto  
      new Contacto(2, "007", "james@bond.com"),
      new Contacto(3, "spiderman", "peter@parker.com"),
    ];
    //atención! asignamos directamente state porque no es promise/asinc
    //de lo contrario lo haríamos mediante setState!!! No ha llamada asincrona (pq somos los primeros), pq los datos estan aqui (no de he llamar a los datos )
    this.state = {// lo asignamos al state
      contactos: contactosInicio,
      ultimoContacto: 3  // porque he creado 3 elementos 
    };
  }
  this.props.guardaContacto({
    nombre: this.state.nombre,
    email: this.state.email,
    id: this.state.id // aqui se cual sera
});
  guardaContacto(datos) {
    //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
    if (datos.id===0){// si es uno nuevo me llegara como 0, Paraa nuevo contacto 
      datos.id = this.state.ultimoContacto+1; // el utlitmo contacto +1 
      this.setState({ultimoContacto: datos.id});
    }
    // comprobaciones adicionales... email ya existe? datos llenos?
    // si todo ok creamos contacto y lo añadimos a la lista
    let nuevo = new Contacto(datos.id, datos.nombre, datos.email); // creo un contacto nuevo 
    // si contacto existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo contacto o desde modifica contacto
    let nuevaLista = this.state.contactos.filter( el => el.id!==nuevo.id);// obtengo una nueva lista quitando el que estoy añadiendo  
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);  // si es nuevo añadelo 
    // finalmente actualizamos state
    this.setState({contactos: nuevaLista}); 
  }


  eliminaContacto(idEliminar) {
    //creamos lista a partir de state.contactos, sin el contacto con el id recibido
    let nuevaLista = this.state.contactos.filter( el => el.id!==idEliminar); // obtengo una nueva lista 
    //asignamos a contactos
    this.setState({contactos: nuevaLista});
    
  }

  render() {

    // como no hay al fetch no ponemos lo de cargando datos..., no llamada externa ,normalmente se pone si la llamada es asincrona( es decir que hay un orden en la actualizacion de datos) 
   // solo saltara el mensaje si no hay datos -, preveer que la BD no responda 
    // si modificamos el state se vuelve a ejecutar en render
    return (  
      <BrowserRouter>
        <Container>
          <Row>
            <Col xs="3">
              <ul className="list-unstyled">  
                <li> <Link to="/">Inicio</Link> </li>
                <li> <Link to="/lista">Contactos</Link> </li>
                <li> <Link to="/nuevo">Añadir</Link> </li>
              </ul>

            </Col>
          </Row>
          <Row>
            <Col xs="8">
              <Switch>
                <Route exact path="/" component={Inicio} />
                <Route path="/lista" render={()=><Lista asdf={this.state.contactos} />} />
                <Route path="/nuevo" render={() => <NuevoContacto guardaContacto={this.guardaContacto} />} />
                <Route path="/modifica/:idContacto" render={(props) => <ModificaContacto contactos={this.state.contactos} guardaContacto={this.guardaContacto} {...props} />} /> 
                <Route path="/elimina/:idContacto" render={(props) => <EliminaContacto contactos={this.state.contactos} eliminaContacto={this.eliminaContacto} {...props} />}  />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>
          <Row>
            <Button onClick={this.loadData}> cargar datos</Button>
            <div>___</div>
            <Button onClick={this.saveData}> guardar datos</Button>
          </Row>
        </Container>
      </BrowserRouter>
    );
  }
}
// en un hijo no puedo modificar las props, solo del madre-> para modificar:referencia a su function y cambiar
//<Route path="/nuevo" render={() => <NuevoContacto guardaContacto={this.guardaContacto} />} />  // le paso una finction para modificar los contactos
//<Route path="/elimina/:idContacto" render={(props) => <EliminaContacto contactos={this.state.contactos} eliminaContacto={this.eliminaContacto} {...props} />}  /> // enlace a contacto y eliminaContacto para cuando el usuario aprieta el boton
// {...props} añadir las propiedades de  idContacto :  idContato me llega a traves de la props del render y estas propiedades las añado 
// solo cuando tengo una varibale  Route path="/elimina/:idContacto" poner {... props}



//                  <td><Link to={"/modifica/" + contacto.id}>Editar</Link></td> de lista .jsx ,  
// <Route path="/nuevo" render={() => <NuevoContacto guardaContacto={this.guardaContacto} />} />  si el id es 0 sabra qu es nuevo, si es diferente se habra de modificar 

/**
 * MANDAMIENTO S
 * CON ARRAYS
 *r=[/*"a"*//*"b"];
 * p=r
 * p.push("d")-->solo existe un arrayy, no puedo añadir, tengo que reemplazar el array por otro
 *aunque cambies el nombre del array, sugue siendo el mismo array, luego no reemplazas,sobreescribir
 * neceistamos crear un nuevo array-->expandir
 * r=["a","b"];
 * p=[...r]--> crea un nuevo array con los objetos del array r 
 * 
 * eliminar: con filter, map ya que creamos un array nuevo
 * CON OBJETOS
 * nuevaCiudad(nuevaCiudad){
 	let nuevoUsuario =  JSON.parse(JSON.stringify(this.state.usuario));
nuevoUsuario.ciudad = nuevaCiudad;
this.setState({ usuario: nuevoUsuario });
}
stringify: OBJETO  A UN STRING
parse: STRING A OBJETO, crea un objeto nuevo */