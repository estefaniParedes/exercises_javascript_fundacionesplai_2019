
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button} from 'reactstrap';


class EliminaContacto extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idContacto * 1;// la propiedad que me lega a traves de l aurl 
        let actual = this.props.contactos.filter(el => el.id===id)[0];// aplicar filtro a la lista y quedate con aquellos con las que el id del elemnto sea igual al que te paso y de esta lista de aui obten el elemento inicial , el primeo elemento de esta lista de uno 
                
        this.state = { // me guardo los datos de lo que quieren eliminar, para desdepues preguntarle siq quiere eliminar
            nombre: actual.nombre,
            email: actual.email,
            id: actual.id,
            volver: false  // pulse un boton poner true
        };

        this.eliminar = this.eliminar.bind(this);
        this.noeliminar = this.noeliminar.bind(this);

    }

    eliminar(){
        this.props.eliminaContacto(this.state.id);
        this.setState({volver: true});
    }


    noeliminar(){ // no eliminar 
        this.setState({volver: true});
    }

    render() {

        if (this.state.volver === true) {  // no estara al 1r vez 
            return <Redirect to='/lista' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.nombre}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.noeliminar} color="success">No</Button> // volver es no elimnar, no confundir 
            </>

        );
    }
}






export default EliminaContacto;
