import React from 'react';
import {  NavLink } from "react-router-dom"; // para que no se recarge a pagina, cambia el active del enlace y puedes darle propiedades distintas
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav, // no poner el NavLink de reactsrap porque te recarga la pagina otra vez, por eso lo cojo de react-router-dom
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';

export default class NavMenu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="primary" light expand="md">
          <NavbarBrand href="/"><i className="fa fa-gears" /> <span>Nano Projectes</span></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink className="link nav-link" to="/">Inici</NavLink>
              </NavItem>
           
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Dades
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavItem>
                      <NavLink className="link nav-link" to="/usuaris">Usuaris</NavLink>
                    </NavItem>
                </DropdownItem>
                  <DropdownItem>
                  <NavItem>
                      <NavLink className="link nav-link" to="/projectes">Projectes</NavLink>
                    </NavItem>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                  <NavItem>
                      <NavLink className="link nav-link" to="/tasques">Tasques</NavLink>
                    </NavItem>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}