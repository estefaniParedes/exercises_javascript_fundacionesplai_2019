import { SERVER } from '../Config';

const MODEL = 'tasca'; // guardo contantes para usarlas luego 
const COLLECTION ="tasques";
const ID_FIELD = 'id';
const FIELDS = [
    /*
    { name: 'id', default: '', type: 'number' },
    { name: 'nombre', default: '', type: 'text' },
    { name: 'caracter', default: '', type: 'text' },*/
    
    { name: 'descripcio', default: '', type: 'text' },
    { name: 'minuts_previstos', default: '', type: 'number' },

];

export default class Tasca {
    
    constructor(data = {}) { 
        
        this.id = (data.id !== undefined && data.id !== null) ? data.id : 0;
        FIELDS.forEach(el => {
            this[el.name] = (data[el.name] !== undefined && data[el.name] !== null) ? data[el.name] : el.default
        });
    }

    static getTitle() {
        return MODEL + 's';
    }

    static getFields() {
        return FIELDS;
    }

    static getModelName() {
        return MODEL;
    }

    static getCollection() {
        return COLLECTION;
    }

    static getAll = () => {
        
        const fetchURL = `${SERVER}/${COLLECTION}`; 
        return new Promise((resolve, reject) => { 
            fetch(fetchURL) 
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => { reject([{ error: err }]); }  );
        });
    };
   
    static getById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL) // aqui ha un get , aunue no lo pongas
                .then(results => results.json())
                .then(data => resolve(new Tasca(data[0])))// creo un nuevo Tascao con los datos que he recibido ; toma el primero de ese array de un solo  objeto 
                .catch(err => reject([{ error: err }]));
        });
    };


    static deleteById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL, { method: 'DELETE' }) // aui hacce un delete
                .then(resp => resp.json())
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => { reject([{ error: err }]); });
        });
    };


    save = () => {  // sirve tanto modificar como para guardae 
        return new Promise((resolve, reject) => {
            //si l'objecte no té ID assignem 0
            let idValue = this[ID_FIELD]; // this.id
            if (!idValue) idValue = 0;

        
            let that = JSON.parse(JSON.stringify(this));  
            //si id=0, eliminem id de that
            if (!idValue) {
                delete that.id;
            }

            //mirem si hi ha dades!
            let hiHaDades=false;
            for (var property in that) {
                if (that[property]){
                    hiHaDades=true;
                    break;
                }
            }
            //si no n'hi ha tornem error
            if (!hiHaDades){
                reject({ error: "nodata" });
                return;
            }
          
        
            let method = (idValue) ? 'PUT' : 'POST';  //   modificar (necesito saber id objeto) :crear (crear un id)
         
            fetch(`${SERVER}/${COLLECTION}`, {
                method,// method: method  si coinciden , deque forma le voya aenviar los datos : put o post
    
                headers: new Headers({ 'Content-Type': 'application/json' }),
                    body: JSON.stringify(that) // this es la promise el metodo de donde estoy, that: varaiable que viene de fora, es el this pero en otro nivel 
                   
                })
                .then(resp => resp.json()) // recibo la respuesta
                .then((resp) => resolve(resp)) // devuelvo la respuesta 
                .catch(err => reject(err)); // devuelvo el error 

        });

    }


}