import { SERVER } from '../Config';

const MODEL = 'usuari'; // guardo contantes para usarlas luego 
const COLLECTION = MODEL + 's';
const ID_FIELD = 'id';
const FIELDS = [
    { name: 'nom', default: '', type: 'text' },
    { name: 'email', default: '', type: 'text' }, // type segun el tipo de letrasn numero ponere n eel input
    { name: 'telefon', default: '', type: 'text' },
];

export default class Usuari {
    /*
    -en un archivo diferente
    datos=(nombre:"2xx2", telefnono:"3");
    let usuari = new usuari(datos); // creo un nuevo objeto
    -en usuari.js
    // constructor (datos){
        this.nombre=datos.nombre
        this.telefnono=datos.telefono
    }*/
    
    
    // es un constructor generico, solo mosificando FOIELS se modifica en la bd(sincronizar)
    constructor(data = {}) {  //  para no hacer lo anterios , paso directament los datos en forma de objeto 
        
        this.id = (data.id !== undefined && data.id !== null) ? data.id : 0;
        FIELDS.forEach(el => {
            this[el.name] = (data[el.name] !== undefined && data[el.name] !== null) ? data[el.name] : el.default
        });// el nombre es igual al nombre que te paso ponme el que te paso o el de defecto, que es algo vacio
        // porque data={}, si no tiene nada {} me da por defecto el vacio
        //data={}, forma de determinar un parametro por defecto 
    }
//metodos estaticos podemos acceder directamente desde la clase :e ej para cceder: let t1=Usuari.gettitle();
// los metodos estaticos solo estan una vez, , es un unico espacio de memoria, solo existe una vez
/*ejemplo para acceder a un no estatico 
 let u1= new Usuari()
//u1.save;*/
// odeloa que nos devuelvel modelo, fields, ...
    static getTitle() {
        return MODEL + 's';
    }

    static getFields() {
        return FIELDS;
    }

    static getModelName() {
        return MODEL;
    }

    static getCollection() {
        return COLLECTION;
    }
// metodos que accden a la BD
    //getAll demana tots els registres a la API
    static getAll = () => {
        // formato para crear unapromise
        // el promise es un objeto llamo resolve y reject porue lo voy a usar en fetch
        const fetchURL = `${SERVER}/${COLLECTION}`; // dos varibles, server esta en config, de forma que si la cambio se cambia en todo los sitios donde la he usado
        return new Promise((resolve, reject) => { // devuelve una promise ,,, los objetos resolve y reject son objetos que estan dentro de la promise
            fetch(fetchURL) // paso la url , por defecto siempre es get
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => { reject([{ error: err }]); }  );// gestiona un posible error el otro
        });
    };
    /**Usuari.getall()
     * .tehn ()
     * .then()
     * .catch ()---> me devuelve una promise, hago que actue como a nivel de la clase,
     * 
     * PROMISE es un objeto que permite usarse de forma asincrona, el fetch es un promise
     *  */


    //getById demana un registre a la API
    static getById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL) // aqui ha un get , aunue no lo pongas
                .then(results => results.json())
                .then(data => resolve(new Usuari(data[0])))// creo un nuevo usuario con los datos que he recibido ; toma el primero de ese array de un solo  objeto 
                .catch(err => reject([{ error: err }]));
        });
    };


    //deleteById elimina un registre a través de la API
    static deleteById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL, { method: 'DELETE' }) // aui hacce un delete
                .then(resp => resp.json())
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => { reject([{ error: err }]); });
        });
    };



    // Mètode no estàtic, desa objecte actual a BDD, porque quiero usarlo desde el propio objeto 
    // como no es estatioc necesito un objero , new
    /*u1=new Usuari()
    u1.save()
    this :: this es u1
    
    */
    save = () => {  // sirve tanto modificar como para guardae 
        return new Promise((resolve, reject) => {
            //si l'objecte no té ID assignem 0
            let idValue = this[ID_FIELD]; // this.id
            if (!idValue) idValue = 0;

            //eliminem mètodes de "this"
            //JSON.parse(JSON.stringify : cuando deciamos que tenemos que reemplazar el state; 
            // this convertirlo en un string stringly luego convertirlo en un objeto Json
            // para usar el tahat en un ambiente distinto , tambien porque el this contienen una serie de metodos.
            // como convierto el this a  sting quito los metodos, y solo me quedo con LOS atributos, PARA DEPUrar EL OBJETO, le pasamo los datos limpios
            //
            let that = JSON.parse(JSON.stringify(this));  
            //si id=0, eliminem id de that
            if (!idValue) {
                delete that.id;
            }

            //mirem si hi ha dades!
            let hiHaDades=false;
            for (var property in that) {
                if (that[property]){
                    hiHaDades=true;
                    break;
                }
            }
            //si no n'hi ha tornem error
            if (!hiHaDades){
                reject({ error: "nodata" });
                return;
            }
          
            //hi ha dades, seguim
            //decidim mètode per API, PUT modificarà, POST farà nou
            let method = (idValue) ? 'PUT' : 'POST';  //   modificar (necesito saber id objeto) :crear (crear un id)
           //para distinguir si es put o post, si es idvalue>0 hacemos un put, sino un post

            //fem connexió passant dades via JSON
            fetch(`${SERVER}/${COLLECTION}`, {
                method,// method: method  si coinciden , deque forma le voya aenviar los datos : put o post
                //pasar peticion al servidor 
                //header es un objeto es la cabecera
                headers: new Headers({ 'Content-Type': 'application/json' }),
                    body: JSON.stringify(that) // this es la promise el metodo de donde estoy, that: varaiable que viene de fora, es el this pero en otro nivel 
                    // lo convienrto json y se lo paso 
                })
                .then(resp => resp.json()) // recibo la respuesta
                .then((resp) => resolve(resp)) // devuelvo la respuesta 
                .catch(err => reject(err)); // devuelvo el error 

        });

    }


}//prototipe : la clase de l aque deriva el objeto 