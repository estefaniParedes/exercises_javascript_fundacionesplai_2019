import React from 'react';
import './css/mano_i.css';


const Mano = (props) => {
    let imagen = (props.estado) ? <i class="fas fa-hand-point-up" ></i> : <i class="fas fa-hand-middle-finger"></i>;
    return imagen
}

export default Mano;

