import React from "react";

import Foto from './Foto';

import Titulo from './Titulo';
import Interruptor from './Interruptor';
import Luz from './Luz';
import Thumbs from './Thumbs';
/*
import Copia from './Copia';

import Lista from './Lista';
import {CIUTATS} from './Datos';*/


export default () => (
  <>
    <hr />
    <Titulo texto="Ejemplo nano-react" />
    <hr />
    <Foto />
    <Interruptor texto="BOOLEAN" estadoInicial={true} />
    <hr />
    <Luz estadoInicial={false} />
    <hr />
    <hr />
    <Thumbs estadoInicial={false} />
    <hr />
  </>
);

/*


<hr />
<Copia />
<hr />
<Lista items={CIUTATS} />
*/