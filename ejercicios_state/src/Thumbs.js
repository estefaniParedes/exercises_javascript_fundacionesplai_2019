import React from 'react';
import './css/mano.css';
import Mano from './Mano';
import Boton from './Boton';

export default class Thumbs extends React.Component {

    constructor(props){
        super(props);
        this.state={
            movimiento: this.props.estadoInicial,
        }
        this.pulsar = this.pulsar.bind(this);
    }
    pulsar(){
        this.setState({
            movimiento: !this.state.movimiento
        })
    }

    render() {

        return (
            <div className="caja-mano">
                <p>mano</p>
                <Mano estado={this.state.movimiento} />
                <Boton onClick={this.pulsar} estado={this.state.movimiento}/>
            </div>
        );
    }
    
}

/*  <Boton />*/
