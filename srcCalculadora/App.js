import React, { Component } from 'react';

import Calculadora from './componentes/Calculadora'

class App extends Component {
  render() {
    return (
      <React.Fragment> 
        <Calculadora  />
      </React.Fragment>
    );
  }
}

export default App;