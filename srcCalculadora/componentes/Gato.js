import React, { Component } from 'react';
import './css/gatos.css';
export default function Gato(props){
    let url = "http://placekitten.com/"+props.ancho+"/"+props.alto;
    return (
        <div className="gato">
            <img src={url} alt={props.nombre} width={props.ancho} height={props.alto} />
            <p>{props.nombre}</p>
        </div> 
    );

}