import React from 'react';
import Dato from './Dato';
import Boton from './Boton';

class Contador extends React.Component {
    constructor(props){//se ejecuta al arrnacar, lo que se reinicilaiza
        super(props);//pq tenemos extends
        this.state={
            display: this.props.valorInicial
        }
        //atar al contructor y al objeto coooncreto en el que esta;
        this.displayMenos=this.displayMenos.bind(this); //para que entienda que el this del menos y del mas sea el mismo numero
        this.displayMas=this.displayMas.bind(this);
    }
    
    displayMenos(){
        //this.state.display--; no es pot modificaar propo de state
        if(this.state.display>0){

            this.setState({ //this es propiedade del objeto
    
                display: this.state.display-1
            });
        }

    }
        displayMas(){
        //this.state.display--; no es pot modificaar propiedades de state
        //porque cada modificacion dispara un cascada de cambios--> para evitar conflictos de cambios se han de ordenar
        if(this.state.display>0){
        this.setState({
            display: this.state.display+1
        });
         }

    }
  
    render() {
      return (
        <React.Fragment>
        <Boton clicado={this.displayMenos} texto="Menos"/>
        <button onClick={displayMenos}>menos</button>
        <button texto="Menos"/>
        <button texto="Mas"/>
        <h2>{this.state.display}</h2>
        <h2>{this.state.display+1}</h2>
        <h2>{this.state.display+2}</h2>
        <h2>{this.state.display+3}</h2>
        <Dato valor={this.state.display}/>
        <Dato valor={this.state.display+1}/>
        <button onClick={displayMas}>mas</button>
        <Boton clicado={this.displayMenos} texto="Mas"/>
        </React.Fragment>
      );
    }
  }
  export default Contador;
  //las funciones se ponen en el state porque es el que puede cambiar el Dato
  //Boton modifica el state cuando hago onclick y revierte el resultado en Dato
  //Date con els state se redibuja, cambia el valor
//constructor function que llamamos al crear una clase, pasa a component las propriedades, 
//super devuelve las props , solo cuando ponemos extends
//onClick={contadorMenos}
//podemos tener clases y funciones son componentes
//export default fucntion(props)
//class App extends Component no ponemos parametros ponenmos this.props.valorInicial
//para que un valor pueda cambiar -->props-->state